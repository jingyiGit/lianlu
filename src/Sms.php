<?php

namespace Lianlu;

use Lianlu\Common\config;
use Lianlu\Clients\SmsSend;
use Lianlu\Common\LianLuException;
use Lianlu\Clients\Product;
use Lianlu\models\Template;

/**
 * 短信操作
 */
class Sms extends config
{
    /**
     * 短信发送
     * https://www.shlianlu.com/console/document/api_4_2
     *
     * @param string $phone     手机号
     * @param string $content   短信内容
     * @param string $sign_name 可空，短信签名，留空则使用默认签名，格式：【淘宝】
     * @param string $tag       可空，短信标签，格式：abnormal
     * @param int    $task_time 可空，定时短信，设置短信按照预定的时间发送，10位时间戳
     * @return bool|string
     */
    public function send($phone, $content, $sign_name = '', $tag = null, $task_time = null)
    {
        if (!$phone) {
            return $this->setError('手机号不能为空');
        } elseif (!$content) {
            return $this->setError('短信内容不能为空');
        }
        $input = new \Lianlu\models\Sms();
        $input->SetPhoneNumberSet($phone);
        $input->SetSessionContext($content);
        
        if ($sign_name) {
            if (strpos($sign_name, '【') === false) {
                $sign_name = '【' . $sign_name . '】';
            }
            $input->SetSignName($sign_name);
        }
        if ($tag) {
            $input->SetTag($tag);
        }
        if ($task_time) {
            if (strlen($task_time) == 10) {
                $task_time = $task_time . '000';
            }
            $input->SetTaskTime($task_time);
        }
        
        try {
            $res = SmsSend::NormalSend($this->cred, $input);
            return $this->handleReturn($res);
        } catch (LianLuException $e) {
            return $this->setError($e->getMessage());
        }
    }
    
    /**
     * 余额查询
     * https://www.shlianlu.com/console/document/api_4_9
     *
     * @return mixed|string
     * @throws Common\LianLuException
     */
    public function getBalance()
    {
        $app = new Product();
        $res = $this->handleReturn($app->Balance($this->cred));
        if ($res['message'] == 'success') {
            return $res['balance'];
        } else {
            return $this->setError($res);
        }
    }
    
    /**
     * 创建短信模板
     * https://www.shlianlu.com/console/document/api_4_13
     *
     * @param string $name    模板名称，如：异常通知
     * @param string $content 模板内容(无需携带签名), 营销短信模板内容格式：内容 + 退订文案，常见退订文案有：退订回T、退订回N、退订回复TD、退回T、拒收回N、取阅回N、退订回N。
     * @param int    $sign_id 签名Id
     * @return false|mixed|string
     * @throws LianLuException
     */
    public function templateCreate($name, $content, $sign_id = 0)
    {
        $template = new Template();
        $template->SetTemplateName($name);
        $template->SetContent($content);
        $template->SetSignId($sign_id);
        $res = $this->handleReturn(Product::SmsTemplateCreate($this->cred, $template));
        if ($res['message'] == 'success') {
            return $res;
        } else {
            return $this->setError($res);
        }
    }
    
    
    /**
     * 短信模板查询
     * https://www.shlianlu.com/console/document/api_4_122
     *
     * @param string $template_id 模板id
     * @return false|mixed|string
     */
    public function templateGetById($template_id)
    {
        $res = $this->handleReturn(Product::TemplateGetById($this->cred, $template_id));
        if ($res['message'] == 'success') {
            return $res;
        } else {
            return $this->setError($res);
        }
    }
}
