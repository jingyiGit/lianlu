<?php

namespace Lianlu;

use Lianlu\Common\config;
use Lianlu\Clients\Product;
use Lianlu\Common\LianLuException;
use Lianlu\Clients\VoiceSend;

/**
 * 语音
 */
class Voice extends config
{
    /**
     * 语音发送
     * https://www.shlianlu.com/console/document/vSend
     *
     * @param string $phone     手机号
     * @param string $content   语音内容
     * @param string $tag       可空，短信标签，格式：abnormal
     * @param int    $task_time 可空，定时短信，设置短信按照预定的时间发送，10位时间戳
     * @return bool|string
     */
    public function send($phone, $content, $tag = null, $task_time = null)
    {
        if (!$phone) {
            return $this->setError('手机号不能为空');
        } elseif (!$content) {
            return $this->setError('通知内容不能为空');
        }
        $input = new \Lianlu\models\Voice();
        $input->SetPhoneNumberSet($phone);
        $input->SetSessionContext($content);
        
        if ($tag) {
            $input->SetTag($tag);
        }
        if ($task_time) {
            if (strlen($task_time) == 10) {
                $task_time = $task_time . '000';
            }
            $input->SetTaskTime($task_time);
        }
        
        try {
            $res = VoiceSend::NormalSend($this->cred, $input);
            return $this->handleReturn($res);
        } catch (LianLuException $e) {
            return $this->setError($e->getMessage());
        }
    }
    
    /**
     * 余额查询
     * https://www.shlianlu.com/console/document/vBanlance
     *
     * @return mixed|string
     * @throws Common\LianLuException
     */
    public function getBalance()
    {
        $app = new Product();
        $res = $this->handleReturn($app->Balance($this->cred));
        if ($res['message'] == 'success') {
            return $res['balance'];
        } else {
            return $this->setError($res);
        }
    }
    
    /**
     * 创建语音模板
     * https://www.shlianlu.com/console/document/voice1
     *
     * @param string $name    模板名称，如：异常通知
     * @param string $content 1、模板如需变量，请在模板文本相应位置插入变量，尊敬的{%变量1%}，您本次登录平台验证码为：{%变量2%}，两分钟内有效
     * @return false|mixed|string
     * @throws LianLuException
     */
    public function templateCreate($name, $content)
    {
        $template = new models\Template();
        $template->SetTemplateType('TXT');
        $template->SetTemplateName($name);
        $template->SetSessionContext($content);
        $res = $this->handleReturn(Product::VoiceTemplateCreate($this->cred, $template));
        if ($res['message'] == 'success') {
            return $res;
        } else {
            return $this->setError($res);
        }
    }
}
