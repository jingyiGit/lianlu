<?php

namespace Lianlu\Common;

/**
 * 配置
 */
class config
{
    protected $cred;
    protected $error = null;
    protected $error_list = [
    ];
    
    public function __construct($config)
    {
        if (!$config['mch_id']) {
            return $this->setError('mch_id 字段不能为空');
        } elseif (!$config['app_id']) {
            return $this->setError('app_id 字段不能为空');
        } elseif (!$config['app_key']) {
            return $this->setError('app_key 字段不能为空');
        }
        $this->cred = new Credential($config['mch_id'], $config['app_id'], $config['app_key']);
        return true;
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    protected function setError($error)
    {
        if (isset($this->error_list[$error['errcode']])) {
            $error['msg'] = $this->error_list[$error['errcode']];
        }
        $this->error = $error;
        return false;
    }
    
    /**
     * 处理返回
     *
     * @param string $result 内容
     * @return mixed
     */
    protected function handleReturn($result)
    {
        if (is_array($result)) {
            return $result;
        } else if ($temp = json_decode($result, true)) {
            return $temp;
        }
        return $result;
    }
}
