<?php

namespace Lianlu\Common;

class Constants
{
    const Version           = "1.1.0";
    const HTTPS             = "https://";
    const HTTP              = "http://";
    const DOMAIN_API        = "api.shlianlu.com/";
    const DOMAIN_API_VOICE  = "apis.shlianlu.com/";
    const SIGNTYPE          = "SignType";
    const MD5               = "MD5";
    const FIELD_SIGN        = "Signature";
    const ContextParamSet   = "ContextParamSet";
    const TemplateParamSet  = "TemplateParamSet";
    const SessionContextSet = "SessionContextSet";
    const PhoneNumberSet    = "PhoneNumberSet";
    const SessionContext    = "SessionContext";
    const PhoneList         = "PhoneList";
    const MchId             = "MchId";
    const TemplateId        = "TemplateId";
    const AppId             = "AppId";
    const Type              = "Type";
    const SignName          = "SignName";
}
