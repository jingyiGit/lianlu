<?php

namespace Lianlu\Clients;

use Lianlu\Common\Constants;
use Lianlu\Common\Credential;
use Lianlu\Common\LianLuException;
use Lianlu\Common\Request;
use Lianlu\Common\Utils;
use Lianlu\models;

class InterSend
{
    private static $version = Constants::Version;
    private static $SignType = Constants::MD5;
    
    private static $prefix = "sms/inter/";
    
    /**
     * @param Credential   $credential
     * @param models\Inter $inter
     * @return bool|string
     * @throws LianLuException
     */
    public static function Send(Credential $credential, models\Inter $inter)
    {
        $url      = Constants::HTTPS . Constants::DOMAIN_API . self::$prefix . "send";
        $inputObj = new models\Inter();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        $inputObj->SetPhoneNumberSet($inter->GetPhoneNumberSet());
        $inputObj->SetTemplateId($inter->GetTemplateId());
        $inputObj->SetTemplateParamSet($inter->GetTemplateParamSet());
        
        if (@$inter->GetTaskTime()) {
            $inputObj->SetTaskTime($inter->GetTaskTime());
        }
        if (@$inter->GetTag()) {
            $inputObj->SetTag($inter->GetTag());
        }
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
}
