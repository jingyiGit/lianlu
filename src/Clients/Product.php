<?php

namespace Lianlu\Clients;

use Lianlu\Common\Constants;
use Lianlu\Common\Credential;
use Lianlu\Common\LianLuException;
use Lianlu\Common\Request;
use Lianlu\Common\Utils;
use Lianlu\models;

class Product
{
    private static $version = Constants::Version;
    private static $SignType = Constants::MD5;
    
    private static $prefix = "sms/";
    
    /**
     * @param Credential $credential
     * @return bool|string
     * @throws LianLuException
     */
    public static function Balance(Credential $credential)
    {
        $url      = Constants::HTTP . Constants::DOMAIN_API . self::$prefix . "product/balance";
        $inputObj = new models\Balance();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
    
    /**
     * @return bool|string
     * @throws LianLuException
     * @var mixed
     */
    public static function SignGet(Credential $credential)
    {
        $url      = Constants::HTTP . Constants::DOMAIN_API . self::$prefix . "product/sign/get";
        $inputObj = new models\Sign();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
    
    /**
     * @param Credential  $credential
     * @param models\Sign $sign
     * @return bool|string
     * @throws LianLuException
     */
    public static function SignCreate(Credential $credential, models\Sign $sign)
    {
        $url      = Constants::HTTP . Constants::DOMAIN_API . self::$prefix . "product/sign/get";
        $inputObj = new models\Sign();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetContent($sign->GetContent());
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
    
    /**
     * @return bool|string
     * @throws LianLuException
     * @var mixed
     */
    public static function TemplateGet(Credential $credential)
    {
        $url      = Constants::HTTP . Constants::DOMAIN_API . self::$prefix . "product/template/get";
        $inputObj = new models\Template();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
    
    public static function TemplateGetById(Credential $credential, $template_id)
    {
        $url      = Constants::HTTP . Constants::DOMAIN_API . self::$prefix . "product/template/getById";
        $inputObj = new models\Template();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        $inputObj->SetTemplateId($template_id);
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
    
    /**
     * @param Credential      $credential
     * @param models\Template $template
     * @return bool|string
     * @throws LianLuException
     */
    public static function SmsTemplateCreate(Credential $credential, models\Template $template)
    {
        $url      = Constants::HTTP . Constants::DOMAIN_API . self::$prefix . "product/template/create";
        $inputObj = new models\Template();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetContent($template->GetContent());
        $inputObj->SetTemplateName($template->GetTemplateName());
        $inputObj->SetSignId($template->GetSignId());
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
    
    /**
     * @param Credential      $credential
     * @param models\Template $template
     * @return bool|string
     * @throws LianLuException
     */
    public static function VoiceTemplateCreate(Credential $credential, models\Template $template)
    {
        $url      = Constants::HTTP . Constants::DOMAIN_API_VOICE . self::$prefix . "voice/template/create";
        $inputObj = new models\Template();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetSessionContext($template->GetSessionContext());
        $inputObj->SetTemplateType($template->GetTemplateType());
        $inputObj->SetTemplateName($template->GetTemplateName());
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
    
    /**
     * @param Credential    $credential
     * @param models\Report $report
     * @return bool|string
     * @throws LianLuException
     */
    public static function Report(Credential $credential, models\Report $report)
    {
        $url      = Constants::HTTP . Constants::DOMAIN_API . self::$prefix . "trade/report";
        $inputObj = new models\Report();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        $inputObj->SetTaskId($report->GetTaskId());
        
        if (@$report->GetPageNo()) {
            $inputObj->SetPageNo($report->GetPageNo());
        }
        if (@$report->GetPageSize()) {
            $inputObj->SetPageSize($report->GetPageSize());
        }
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
    
    /**
     * @param Credential    $credential
     * @param models\Report $report
     * @return bool|string
     * @throws LianLuException
     */
    public static function reply(Credential $credential, models\Report $report)
    {
        $url      = Constants::HTTP . Constants::DOMAIN_API . self::$prefix . "trade/reply";
        $inputObj = new models\Report();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        $inputObj->SetTaskId($report->GetTaskId());
        
        if (@$report->GetPageNo()) {
            $inputObj->SetPageNo($report->GetPageNo());
        }
        if (@$report->GetPageSize()) {
            $inputObj->SetPageSize($report->GetPageSize());
        }
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
}
