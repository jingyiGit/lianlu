<?php

namespace Lianlu\Clients;

use Lianlu\Common\Constants;
use Lianlu\Common\Credential;
use Lianlu\Common\LianLuException;
use Lianlu\Common\Request;
use Lianlu\Common\Utils;
use Lianlu\models;

class SmsSend
{
    private static $version = Constants::Version;
    private static $SignType = Constants::MD5;
    
    private static $prefix = "sms/trade/";
    
    /**
     * @param Credential $credential
     * @param models\Sms $sms
     * @return bool|string
     * @throws LianLuException
     */
    public static function NormalSend(Credential $credential, models\Sms $sms)
    {
        $url      = Constants::HTTPS . Constants::DOMAIN_API . self::$prefix . "normal/send";
        $inputObj = new models\Sms();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        $inputObj->SetPhoneNumberSet($sms->GetPhoneNumberSet());
        $inputObj->SetSignName($sms->GetSignName());
        $inputObj->SetSessionContext($sms->GetSessionContext());
        $inputObj->SetType("1");
        
        if (@$sms->GetTaskTime()) {
            $inputObj->SetTaskTime($sms->GetTaskTime());
        }
        if (@$sms->GetTag()) {
            $inputObj->SetTag($sms->GetTag());
        }
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
    
    /**
     * @param Credential $credential
     * @param models\Sms $sms
     * @return bool|string
     * @throws LianLuException
     */
    public static function PersonalSend(Credential $credential, models\Sms $sms)
    {
        $url      = Constants::HTTPS . Constants::DOMAIN_API . self::$prefix . "personal/send";
        $inputObj = new models\Sms();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        $inputObj->SetSessionContextSet($sms->GetSessionContextSet());
        $inputObj->SetSignName($sms->GetSignName());
        $inputObj->SetContextParamSet($sms->GetContextParamSet());
        $inputObj->SetType("2");
        
        if (@$sms->GetTaskTime()) {
            $inputObj->SetTaskTime($sms->GetTaskTime());
        }
        if (@$sms->GetTag()) {
            $inputObj->SetTag($sms->GetTag());
        }
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
    
    /**
     * @param Credential $credential
     * @param models\Sms $sms
     * @return bool|string
     * @throws LianLuException
     */
    public static function TemplateSend(Credential $credential, models\Sms $sms)
    {
        $url      = Constants::HTTPS . Constants::DOMAIN_API . self::$prefix . "template/send";
        $inputObj = new models\Sms();
        $inputObj->SetAppid($credential->GetAppId());
        $inputObj->SetMch_id($credential->GetMchId());
        $inputObj->SetSignType(self::$SignType);
        $inputObj->SetVersion(self::$version);
        $inputObj->SetTimeStamp(Utils::getMillisecond());
        $inputObj->SetPhoneNumberSet($sms->GetPhoneNumberSet());
        $inputObj->SetTemplateId($sms->GetTemplateId());
        $inputObj->SetTemplateParamSet($sms->GetTemplateParamSet());
        $inputObj->SetType("3");
        
        if (@$sms->GetTaskTime()) {
            $inputObj->SetTaskTime($sms->GetTaskTime());
        }
        if (@$sms->GetTag()) {
            $inputObj->SetTag($sms->GetTag());
        }
        
        $inputObj->SetSign($inputObj, $credential->getAppKey());
        $json = $inputObj->ToJson();
        return Request::postCurl($json, $url);
    }
}
